﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14观察者模式_Observer_
{
    class Program
    {
       //// 题目：猫大叫,两只老鼠开始逃跑,主人醒来,宝宝也醒来了并且哭了起来.
        static void Main(string[] args)
        {
            Cat myCat = new Cat();
            Mouse myMouse1 = new Mouse("mouse1", myCat);
            Mouse myMouse2 = new Mouse("mouse2", myCat);
            Master myMaster = new Master(myCat);
            Baby myBaby = new Baby(myCat);
            myCat.Cry();
        }
    }
}
