﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14观察者模式_Observer_
{
    /// <summary>
    /// 建立模型(目标基类)
    ///  在Observer Pattern(观察者模式)中,此类作为所有Subject(目标)的抽象基类,所有要充当Subject的类(在此事例中为"猫")都继承于此类.
    /// 我们说此类作为模型,用于规划目标(即发布方)所产生的事件,及提供触发事件的方法.  
    /// 此抽象类无抽象方法,主要是为了不能实例化该类对象,确保模式完整性.
    /// 具体实施:
    ///  1.声明委托
    ///  2.声明委托类型事件
    ///  3.提供触发事件的方法
    /// </summary>
    public abstract class ModelBase
    {
        public ModelBase()
        {
        }
        public delegate void SubEventHandler();
        public event SubEventHandler SubEvent;
        protected void Notify()
        {
            //提高执行效率及安全性
            if (this.SubEvent != null)
                this.SubEvent();
        }
    }
    /// <summary>
    /// 具体目标
    /// </summary>
    public class Cat : ModelBase
    {
        public  Cat()
        { }
        public void Cry()
        {
            this.Notify();
        }
    }
}
