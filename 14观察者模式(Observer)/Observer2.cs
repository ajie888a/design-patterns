﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14观察者模式_Observer_
{
    /// <summary>
    /// 在Observer Pattern(观察者模式)中,此类作为所有Observer(观察者)的抽象基类,所有要充当观察者的类(在此事例中为"老鼠"和"人")都继承于此类.
    /// 我们说此类作为观察者基类,用于规划所有观察者(即订阅方)订阅行为.
    /// 1.指定观察者所观察的对象(即发布方).(通过构造器传递)
    ///  2.规划观察者自身需要作出响应方法列表
    ///   3.注册需要委托执行的方法.(通过构造器实现)
    ///</summary>
    public abstract class Observer2
    {
        /// 构造时通过传入模型对象,把观察者与模型关联,并完成订阅.
        public Observer2(ModelBase childModel)
        {
            childModel.SubEvent += new ModelBase.SubEventHandler(Response);
            childModel.SubEvent += new ModelBase.SubEventHandler(Response2);

        }

        /// <summary>
        /// 规划了观察者的一种行为(方法),所有派生于该观察者基类的具体观察者都通过覆盖该方法来实现作出响应的行为.
        /// </summary>
        public abstract void Response();
        public abstract void Response2();
    }

    /// <summary>
    /// 具体观察者－－宝宝
    /// </summary>
    public class Baby : Observer2
    {
        public Baby(ModelBase childModel):base(childModel)
        {
        }
        public override void Response()
        {
            System.Console.WriteLine("宝宝醒来");
        }
        public override void Response2()
        {
            System.Console.WriteLine("开始哭闹");
        }
    }
}
