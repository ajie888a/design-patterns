﻿using Com.Design.Gof.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24访问者模式_Visitor_
{
    class Program
    {
        static void Main(string[] args)
        {
            //三个小伙子，开车到长隆欢乐世界 游玩， 每个人想玩的项目都不一样。
            List<ITourist> list = new List<ITourist> {
             new TouristOne(),
             new TouristTwo(),
             new TouristThree()
            };
            //车开到了长隆  南大门，长隆到了
            ChangLongHappyWorld happyWorld = new ChangLongHappyWorld();

            //开始  游玩 长隆啦！！
            foreach (var visit in list)
            {
                visit.Play(happyWorld);
                Console.WriteLine("------------------------------------------------");
            }
            Console.ReadKey();
        }
    }
}
