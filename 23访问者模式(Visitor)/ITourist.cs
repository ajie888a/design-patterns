﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Design.Gof.Visitor
{
    public interface ITourist
    {
        /// <summary>
        /// 游玩
        /// </summary>
        /// <param name="happyWorld">长隆欢乐世界</param>
         void Play(ChangLongHappyWorld happyWorld);
    }
}
