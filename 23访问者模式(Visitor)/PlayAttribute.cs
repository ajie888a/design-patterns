﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Design.Gof.Visitor
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class PlayAttribute : Attribute
    {
        private string _PlayItem;
        /// <summary>
        /// 游玩的项目
        /// </summary>
        public string PlayItem
        {
            get { return _PlayItem; }
            set { _PlayItem = value; }
        }
    }
}
