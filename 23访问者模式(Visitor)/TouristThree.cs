﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Design.Gof.Visitor
{
    /// <summary>
    /// 旅游者 3   想玩：四维影院,垂直极限，U型滑板
    /// </summary>
    public class TouristThree : ITourist
    {
        /// <summary>
        /// 四维影院
        /// </summary>
        [PlayAttribute(PlayItem = "AirPolice")]
        public void Play_Cinema4D() {
            Console.WriteLine("我是游客3，我现在玩的是：四维影院");
        }
        /// <summary>
        /// 垂直极限
        /// </summary> 	
        [PlayAttribute(PlayItem = "VerticalLimit")]
        public void Play_VerticalLimit()
        {
            Console.WriteLine("我是游客3，我现在玩的是：垂直极限");
        }
        /// <summary>
        /// 超级水战
        /// </summary>
        [PlayAttribute(PlayItem = "UShapeSkateboard")]
        public void Play_UShapeSkateboard()
        {
            Console.WriteLine("我是游客3，我现在玩的是：U型滑板");
        }
        public void Play(ChangLongHappyWorld happyWorld)
        {
            happyWorld.visit(this);
        }
    }
}
