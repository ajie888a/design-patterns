﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
namespace Com.Design.Gof.Visitor
{
    /// <summary>
    /// 长隆欢乐世界
    /// </summary>
    public class ChangLongHappyWorld
    {
        /// <summary>
        /// 接待各个访问者
        /// </summary>
        /// <param name="visitor"></param>
        public void visit(ITourist visitor) {
            //每个旅游者想玩的项目不一样。使用反射，方便调用
            MethodInfo[] method = visitor.GetType().GetMethods();
            foreach (MethodInfo m in method) {
                object[] property= m.GetCustomAttributes(false);
                string customerAttribute = null;
                if (property.Length>0) {
                   customerAttribute = property[0].ToString();
                }
                if (customerAttribute == "Com.Design.Gof.Visitor.PlayAttribute")
                {
                    m.Invoke(visitor, new object[] { });
                }
            }
        }
    }
}
