﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Design.Gof.Visitor
{
    /// <summary>
    /// 旅游者 1   想玩：十环过山车,龙卷风暴，梦幻旋马
    /// </summary>
    public class TouristOne : ITourist
    {
        /// <summary>
        /// 十环过山车
        /// </summary>
        [PlayAttribute(PlayItem = "TenthRingRollerCoaster")]
        public void Play_TenthRingRollerCoaster() {
            Console.WriteLine("我是游客1，我现在玩的是：十环过山车");
        }
        /// <summary>
        /// 龙卷风暴
        /// </summary>
         [PlayAttribute(PlayItem = "TornadoStorm")]
        public void Play_TornadoStorm()
        {
            Console.WriteLine("我是游客1，我现在玩的是：龙卷风暴");
        }
        /// <summary>
        /// 梦幻旋马
        /// </summary>
       [PlayAttribute(PlayItem = "DreamHorse")]
        public void Play_DreamHorse()
        {
            Console.WriteLine("我是游客1，我现在玩的是：梦幻旋马");
        }
        public void Play(ChangLongHappyWorld happyWorld)
        {
            happyWorld.visit(this);
        }
    }
}
