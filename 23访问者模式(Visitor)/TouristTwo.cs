﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Design.Gof.Visitor
{
    /// <summary>
    /// 旅游者 2   想玩：空中警察,欢乐摩天轮，超级水战
    /// </summary>
    public class TouristTwo : ITourist
    {
        /// <summary>
        /// 空中警察
        /// </summary>
        [PlayAttribute(PlayItem = "AirPolice")]
        public void Play_AirPolice() {
            Console.WriteLine("我是游客2，我现在玩的是：空中警察");
        }
        /// <summary>
        /// 欢乐摩天轮
        /// </summary>
        [PlayAttribute(PlayItem = "FerrisWheel")]
        public void Play_FerrisWheel()
        {
            Console.WriteLine("我是游客2，我现在玩的是：欢乐摩天轮");
        }
        /// <summary>
        /// 超级水战
        /// </summary>
        [PlayAttribute(PlayItem = "SuperWater")]
        public void Play_SuperWater()
        {
            Console.WriteLine("我是游客2，我现在玩的是：超级水战");
        }
        public void Play(ChangLongHappyWorld happyWorld)
        {
            happyWorld.visit(this);
        }
    }
}
