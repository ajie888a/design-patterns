﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    public class Divide : Operation
    {
        public override double GetResult()
        {
            return NumberA / NumberB;
        }
    }
}
