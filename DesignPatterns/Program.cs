﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Operation operateion = OperationFactory.CreateOperate("+");
            operateion.NumberA = 10;
            operateion.NumberB = 5;
            Console.WriteLine(operateion.GetResult());
            Console.ReadKey();
        }
    }
}
