﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10组合模式_Composite_
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractFile file1, file2, folder1, file3, folder2, folder3;
            folder1 = new Folder("我的视频");
            folder2 = new Folder("我的图片");
            folder3 = new Folder("我的资料");

            file1 = new TextFile("文本1");

            file2 = new ImageFile("图像2");
            file3 = new TextFile("文本2");
            folder1.Add(file1);
            folder2.Add(file2);
            folder2.Add(file3);
            folder2.KillVirue();
            Console.ReadLine();
        }
    }
}
