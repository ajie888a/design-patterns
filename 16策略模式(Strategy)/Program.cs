﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16策略模式_Strategy_
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee employee = new Employee(new EmployeeSalary());
            Console.WriteLine("Employee salary is:{0}", employee.GetSalary().ToString());
            employee.Salary = new ManagerSalary();
            Console.WriteLine("Employee salary is:{0}", employee.GetSalary().ToString());

            Console.ReadLine();
        }
    }
}
