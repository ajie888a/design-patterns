﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16策略模式_Strategy_
{
    public class ManagerSalary : Salary
    {
        public int Caculator()
        {
            return 1000;
        }
    }
    public class EmployeeSalary : Salary
    {
        public int Caculator()
        {
            return 800;
        }
    }
}
