﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16策略模式_Strategy_
{
    public class Employee
    {
        public Salary Salary { get; set; }
        public Employee(Salary salary)
        {
            Salary = salary;
        }
        public int GetSalary()
        {
            return Salary.Caculator();
        }
    }
}
