﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22迭代器模式_Iterator_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("从包含自定义迭代器的集合类中枚举元素\n");
            //初始化自定义集合
            MyDocuments document = new MyDocuments();
            document.Add("依莲");
            document.Add("玉珈");
            document.Add("徐长今");
            document.Add("宁雨昔");
            document.Add("安碧如");
            document.Add("洛凝");
            document.Add("萧玉若");

            //通过带有自定义迭代器的类枚举元素
            foreach (string s in document)
                Console.Write(string.Format(" {0} ", s));

            Console.WriteLine("\n**********************************************\n");
            Console.WriteLine("从自定义迭代器方法中枚举元素\n");

            List<string> docs = new List<string>();
            docs.Add("依莲");
            docs.Add("玉珈");
            docs.Add("徐长今");
            docs.Add("宁雨昔");
            docs.Add("安碧如");
            docs.Add("洛凝");
            docs.Add("萧玉若");

            //通过自定义迭代器方法中枚举元素
            foreach (string s in Documents(docs))
                Console.Write(string.Format(" {0} ", s));

            Console.ReadKey();
        }

        /// <summary>
        /// 迭代器方法
        /// </summary>
        static IEnumerable Documents(List<string> docs)
        {
            foreach (string s in docs)
            {
                //当yield return语句执行后，当前位会被
                //保存下来，下一次执行会从当前位开始
                yield return s;
            }
        }

    }
}
