﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22迭代器模式_Iterator_
{
    /// <summary>
    /// 包含迭代器的自定义集合类
    /// </summary>
    public class MyDocuments : IEnumerable
    {
        private List<string> docs = new List<string>();

        public void Add(string s)
        {
            docs.Add(s);
        }

        #region IEnumerable 成员

        public IEnumerator GetEnumerator()
        {
            foreach (string s in docs)
            {
                //当yield return语句执行后，当前位会被
                //保存下来，下一次执行会从当前位开始

                yield return s;
            }

        }

        #endregion/**/
    }
}
