﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    /// <summary> 
    /// 灯光 
    /// </summary> 
    public class Light
    {
        public void OpenLight()
        {
            Console.WriteLine("打开灯光");
        }
        public void CloseLight()
        {
            Console.WriteLine("关闭灯光");
        }
    }
}
