﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    /// <summary> 
    /// 定义一个外观 
    /// </summary> 
    public class MovieFacade
    {
        /// <summary> 
        /// 在外观类中必须保存有子系统中各个对象 
        /// </summary> 
        private Projector projector;
        private Amplifier amplifier;
        private Screen screen;
        private DVDPlayer dvdPlayer;
        private Light light;
        public MovieFacade()
        {
            projector = new Projector();
            amplifier = new Amplifier();
            screen = new Screen();
            dvdPlayer = new DVDPlayer();
            light = new Light();
        }
        /// <summary> 
        /// 打开电影 
        /// </summary> 
        public void OpenMovie()
        {
            //先打开投影仪 
            projector.OpenProjector();
            //再打开功放 
            amplifier.OpenAmplifier();
            //再打开屏幕 
            screen.OpenScreen();
            //再打开 DVD 
            dvdPlayer.OpenDVDPlayer();
            //再打开灯光 
            light.OpenLight();
        }
        /// <summary> 
        /// 关闭电影 
        /// </summary> 
        public void CloseMovie()
        {
            //关闭投影仪 
            projector.CloseProjector();
            //关闭功放 
            amplifier.CloseAmplifier();
            //关闭屏幕 
            screen.CloseScreen();
            //关闭 DVD 
            dvdPlayer.CloseDVDPlayer();
            //关闭灯光 
            light.CloseLight();
        }
    }
}
