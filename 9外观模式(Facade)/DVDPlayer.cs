﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    /// <summary> 
    /// DVD播放器 
    /// </summary> 
    public class DVDPlayer
    {
        public void OpenDVDPlayer()
        {
            Console.WriteLine("打开 DVD 播放器");
        }
        public void CloseDVDPlayer()
        {
            Console.WriteLine("关闭 DVD 播放器");
        }
    }
}
