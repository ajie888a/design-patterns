﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    public class Screen
    {
        public void OpenScreen()
        {
            Console.WriteLine("打开屏幕");
        }
        public void CloseScreen()
        {
            Console.WriteLine("关闭屏幕");
        }
    }
}
