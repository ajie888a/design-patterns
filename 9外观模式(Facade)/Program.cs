﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    class Program
    {
        static void Main(string[] args)
        {
            MovieFacade movie = new MovieFacade();
            Projector projector = new Projector();

            //首先是观看电影 
            movie.OpenMovie();
            Console.WriteLine();

            //然后是将投影仪模式调到宽屏模式 
            projector.SetWideScreen();
            //再将投影仪模式调回普通模式 
            projector.SetStandardScreen();
            Console.WriteLine();

            //最后就是关闭电影了 
            movie.CloseMovie();
            Console.ReadKey();
        }
    }
}
