﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    /// <summary> 
    /// 功放机 
    /// </summary> 
    public class Amplifier
    {
        public void OpenAmplifier()
        {
            Console.WriteLine("打开功放机");
        }
        public void CloseAmplifier()
        {
            Console.WriteLine("关闭功放机");
        }
    }
}
