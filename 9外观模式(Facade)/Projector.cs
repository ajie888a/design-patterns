﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9外观模式_Facade_
{
    /// <summary> 
    /// 投影仪 
    /// </summary> 
    public class Projector
    {
        public void OpenProjector()
        {
            Console.WriteLine("打开投影仪");
        }
        public void CloseProjector()
        {
            Console.WriteLine("关闭投影仪");
        }
        public void SetWideScreen()
        {
            Console.WriteLine("投影仪状态为宽屏模式");
        }
        public void SetStandardScreen()
        {
            Console.WriteLine("投影仪状态为标准模式");
        }
    }
}
