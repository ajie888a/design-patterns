﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19中介者模式_Mediator_
{
    // 抽象牌友类
    public abstract class AbstractCardPartner
    {
        public int MoneyCount { get; set; }

        protected AbstractCardPartner()
        {
            MoneyCount = 0;
        }

        public abstract void ChangeCount(int count, AbstractCardPartner other);
    }
    // 牌友A类
    public class ParterA : AbstractCardPartner
    {
        public override void ChangeCount(int count, AbstractCardPartner other)
        {
            this.MoneyCount += count;
            other.MoneyCount -= count;
        }
    }

    // 牌友B类
    public class ParterB : AbstractCardPartner
    {
        public override void ChangeCount(int count, AbstractCardPartner other)
        {
            this.MoneyCount += count;
            other.MoneyCount -= count;
        }
    }
}
