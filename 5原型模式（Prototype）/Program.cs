﻿using _5原型模式_Prototype_.序列化深复制;
using _5原型模式_Prototype_.浅复制实现;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
namespace _5原型模式_Prototype_
{
    class Program
    {
        static void Main(string[] args)
        {
            //序列化克隆
            //浅表克隆，原来创建的对象值会改变
            Person p = new Person();
            Person p1 = p.Clone() as Person;
            p1.CurrentEmployee = "user";
            p1.Member.Id = 1;
            p1.Member.Name = "pp1";
            //深度克隆,原来创建的对象值不会改变
            Person p2 = p.Clone() as Person;
            Person p3 = SerializeHelper.Derializable<Person>(SerializeHelper.Serializable(p2));
            //或者简写
            //Person p3 = SerializeHelper.DeepClone<Person>(p2);
            p3.Member.Id = 6;
            p3.Member.Name = "dd3";
            Console.Read();






            //  TestShallowCopy();   //浅复制
            //    TestDeepCopy();// 深复制
            Console.ReadKey();
        }

        /// <summary>
        /// 深复制
        /// </summary>
        [TestMethod]
        public static void TestDeepCopy()
        {
          深复制实现.Resume myFirstResume = new 深复制实现.Resume
            {
                Age = 29,
                Name = "Kevin Wang",
                Sex = "男",
            };
            myFirstResume.SetWorkExperience(new DateTime(2006, 7, 1), new DateTime(2007, 7, 1), "My First Company", "Software Engineer");
            深复制实现.Resume mySecondResume = (深复制实现.Resume)myFirstResume.Clone();
            mySecondResume.SetWorkExperience(new DateTime(2007, 8, 1), new DateTime(2008, 8, 1), "My Second Company", "Software Engineer");

            深复制实现.Resume myThirdResume = (深复制实现.Resume)myFirstResume.Clone();
            myThirdResume.SetWorkExperience(new DateTime(2008, 8, 1), new DateTime(2009, 8, 1), "My Third Company", "Senior Software Engineer");
            Assert.AreEqual("My First Company", myFirstResume.WorkExperience.Company);
            Assert.AreEqual("My Second Company", mySecondResume.WorkExperience.Company);
            Assert.AreEqual("My Third Company", myThirdResume.WorkExperience.Company);
        }


        /// <summary>
        /// 浅复制
        /// </summary>
        [TestMethod]
        public static void TestShallowCopy()
        {
            Resume myFirstResume = new Resume
            {
                Age = 29,
                Name = "Kevin Wang",
                Sex = "男",
            };
            myFirstResume.SetWorkExperience(new DateTime(2006, 7, 1), new DateTime(2007, 7, 1), "My First Company", "Software Engineer");

            Resume mySecondResume = (Resume)myFirstResume.Clone();
            mySecondResume.SetWorkExperience(new DateTime(2007, 8, 1), new DateTime(2008, 8, 1), "My Second Company", "Software Engineer");

            Resume myThirdResume = (Resume)myFirstResume.Clone();
            myThirdResume.SetWorkExperience(new DateTime(2008, 8, 1), new DateTime(2009, 8, 1), "My Third Company", "Senior Software Engineer");

            Assert.AreEqual("My First Company", myFirstResume.WorkExperience.Company);
            Assert.AreEqual("My Second Company", mySecondResume.WorkExperience.Company);
            Assert.AreEqual("My Third Company", myThirdResume.WorkExperience.Company);
       //     这里期望的是三个断言都能运行成功，但是却是失败的，原因是：由于我们使用的是浅复制，所以myFirstResume, mySecondResume 和 myThirdResume引用的是同一个对象，因此最终的结果是 三个简历的WorkExperience.Company都是“My Third Company".
        }

    }
}
