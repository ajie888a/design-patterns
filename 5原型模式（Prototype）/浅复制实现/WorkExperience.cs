﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5原型模式_Prototype_.浅复制实现
{
    /// <summary>
    /// 工作经历类
    /// </summary>
    public class WorkExperience
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
    }

}
