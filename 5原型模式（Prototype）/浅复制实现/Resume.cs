﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5原型模式_Prototype_.浅复制实现
{
    /// <summary>
    /// 实现了ICloneable接口的简历类
    /// </summary>
    public class Resume : ICloneable
    {
        public Resume()
        {
            mWorkExperience = new WorkExperience();
        }

        private string mName;
        private string mSex;
        private int mAge;
        private WorkExperience mWorkExperience;

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public string Sex
        {
            get { return mSex; }
            set { mSex = value; }
        }

        public int Age
        {
            get { return mAge; }
            set { mAge = value; }
        }
        /// <summary>
        /// 关联了一个引用类型
        /// </summary>
        public WorkExperience WorkExperience
        {
            get { return mWorkExperience; }
        }

        public void SetWorkExperience(DateTime startDate, DateTime endDate, string company, string position)
        {
            this.mWorkExperience.Company = company;
            this.mWorkExperience.EndDate = endDate;
            this.mWorkExperience.StartDate = startDate;
            this.mWorkExperience.Position = position;
        }

        /// <summary>
        /// 实现ICloneable接口的Clone方法
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            // .Net 为我们提供的浅复制对象的方法
            return this.MemberwiseClone();
        }
    }
  

}
