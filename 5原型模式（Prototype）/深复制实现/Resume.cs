﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5原型模式_Prototype_.深复制实现
{
    /// <summary>
    /// 实现了ICloneable接口的简历类
    /// </summary>
    public class Resume : ICloneable
    {
        public Resume()
        {
            mWorkExperience = new WorkExperience();
        }

        /// <summary>
        /// 这里使用一个私有的构造函数来对其连接到的引用类型进行复制
        /// </summary>
        /// <param name="workExperience"></param>
        private Resume(WorkExperience workExperience)
        {
            this.mWorkExperience = (WorkExperience)workExperience.Clone();
        }

        private string mName;
        private string mSex;
        private int mAge;
        private WorkExperience mWorkExperience;

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public string Sex
        {
            get { return mSex; }
            set { mSex = value; }
        }

        public int Age
        {
            get { return mAge; }
            set { mAge = value; }
        }

        public WorkExperience WorkExperience
        {
            get { return mWorkExperience; }
        }

        /// <summary>
        /// 设置功过经历
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="company"></param>
        /// <param name="position"></param>
        public void SetWorkExperience(DateTime startDate, DateTime endDate, string company, string position)
        {
            this.mWorkExperience.Company = company;
            this.mWorkExperience.EndDate = endDate;
            this.mWorkExperience.StartDate = startDate;
            this.mWorkExperience.Position = position;
        }

        /// <summary>
        /// 实现ICloneable接口的Clone方法
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            // 这里不再使用MemberwiseClone方法进行复制了，而是新创建了一个全新的简历。它完全是在内部实现的，外部不用关心它的实现
            Resume newResume = new Resume();
            newResume.mSex = this.mSex;
            newResume.mName = this.mName;
            newResume.mAge = this.mAge;

            return newResume;
        }
    }

}
