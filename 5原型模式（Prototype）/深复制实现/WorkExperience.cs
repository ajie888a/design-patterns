﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5原型模式_Prototype_.深复制实现
{
    public class WorkExperience : ICloneable
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }

        public object Clone()
        {
            // 使用.Net 为我们提供的浅复制对象的方法，因为这里已经没有引用对象了（string虽然是引用类型，但.NET为我们做了特别处理，可以像值类型一样使用它）。
            return this.MemberwiseClone();
        }
    }

}
