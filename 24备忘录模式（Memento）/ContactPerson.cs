﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24备忘录模式_Memento_
{
    // 联系人
    class ContactPerson
    {
        public string Name { get; set; }
        public string MobileNum { get; set; }
    }
}
