﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.CSharp;



namespace _21解释器模式_Interpreter_
{
    public partial class Interpreter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string chinese = "{数据库}[信息](获取)";
            Context context = new Context(chinese);

            List<AbstractExpression> l = new List<AbstractExpression>();
            l.Add(new DatabaseExpression());
            l.Add(new ObjectExpression());
            l.Add(new MethodExpression());

            foreach (AbstractExpression exp in l)
            {
                exp.Interpret(context);
            }

            Assembly assembly = Assembly.Load("Pattern.Interpreter");
            MethodInfo method = assembly.GetType("Pattern.Interpreter." + context.Output.Split('.')[0]).GetMethod(context.Output.Split('.')[1].Replace("()", ""));
            object obj = method.Invoke(null, null);

            List<MessageModel> m = (List<MessageModel>)obj;

            Response.Write("中文语法：" + chinese);
            Response.Write("<br />");
            Response.Write("解释后的C#代码：" + context.Output);
            Response.Write("<br />");
            Response.Write("执行结果：" + m[0].Message + " " + m[0].PublishTime.ToString());
        }
    }
    }
//运行结果
//中文语法：{数据库}[信息](获取)
//解释后的C#代码：SqlMessage.Get()
//执行结果：SQL方式获取Message 2007-5-1 8:48:07 
