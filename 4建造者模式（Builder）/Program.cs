﻿using _4建造者模式_Builder_.建造者模式实践应用;
using _4建造者模式_Builder_.易理解;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4建造者模式_Builder_
{
    class Program
    {
        static void Main(string[] args)
        {
            Director1 director = new Director1();

            director.Constructor(new Benz());

            director.Constructor(new BMW());

            Console.ReadLine();
        }
        static void Main1(string[] args)
        {
            // Create director and builders
            Director director = new Director();

            Builder b1 = new ConcreteBuilder1();
            Builder b2 = new ConcreteBuilder2();

            // Construct two products
            director.Construct(b1);
            Product p1 = b1.GetResult();
            p1.Show();

            director.Construct(b2);
            Product p2 = b2.GetResult();
            p2.Show();
            Console.ReadKey();
        }
        static void Main2(string[] args)
        {
            VehicleBuilder builder;
            // Create shop with vehicle builders
            Shop shop = new Shop();

            // Construct and display vehicles
            builder = new ScooterBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new CarBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new MotorCycleBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();
            Console.ReadKey();
        }
    }
}
