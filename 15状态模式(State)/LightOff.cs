﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15状态模式_State_
{
    public class LightOff : LightState
    {
        public void PressSwitch(Light light)
        {
            Console.WriteLine("Light On");
            light.State = new LightOn();
        }
    }
}
