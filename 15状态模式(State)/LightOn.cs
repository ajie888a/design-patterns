﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15状态模式_State_
{
    public class LightOn : LightState
    {
        public void PressSwitch(Light light)
        {
            Console.WriteLine("Light Off");
            light.State = new LightOff();
        }
    }

}
