﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 抽象工厂模式_Abstract_Factory_
{
    #region 中国兵工厂
    abstract class ChinaFactory
    {
        //装甲车制造车间
        public abstract Tank CreateTank();
        //炸弹知道车间
        public abstract Bomb CreateBomb();
    }
    //兵工厂前期制造
    class QianqiFactory : ChinaFactory
    {
        public override Bomb CreateBomb()
        {
            //导弹
            return new DaodanBomb();
        }
        public override Tank CreateTank()
        {
            //越野车
            return new YueyeTank();
        }
    }
    //兵工厂后期制造
    class HouqiFactory : ChinaFactory
    {
        public override Bomb CreateBomb()
        {
            //核弹
            return new HedanBomb();
        }
        public override Tank CreateTank()
        {
            //主站坦克
            return new ZhuzhanTank();
        }
    }
    #endregion

}
