﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 抽象工厂模式_Abstract_Factory_
{
    class Program
    {
        static void Main(string[] args)
        {
            //大战前期
            ChinaFactory qianqiMake = new QianqiFactory();
            Make qianqi = new Make(qianqiMake);
            qianqi.WarStar();

            //点任意键，进行后期攻势！！
            Console.ReadKey();

            //大战后期
            ChinaFactory houqiMake = new HouqiFactory();
            Make houqi = new Make(houqiMake);
            houqi.WarStar();

            Console.WriteLine("钓鱼岛是中国的，神圣不可侵犯！小邪恶国家，滚开！！");
            Console.ReadLine();

        }
    }
}
