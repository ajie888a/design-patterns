﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 抽象工厂模式_Abstract_Factory_
{
    #region 炸弹系列
    abstract class Bomb
    {
        abstract public void BaoZha();
    }
    /// <summary>
    /// 导弹
    /// </summary>
    class DaodanBomb : Bomb
    {
        public override void BaoZha()
        {
            Console.WriteLine("我是一颗中国造导弹，来轰炸不老实的小邪恶国家！");
        }
    }
    /// <summary>
    /// 核弹
    /// </summary>
    class HedanBomb : Bomb
    {
        public override void BaoZha()
        {
            Console.WriteLine("我是一颗中国造核弹，来轰炸不老实的小邪恶国家！");
        }
    }
    #endregion

}
