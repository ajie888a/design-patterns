﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 抽象工厂模式_Abstract_Factory_
{
    #region 坦克系列
    abstract class Tank
    {
        abstract public void Go();
    }
    /// <summary>
    /// 越野车
    /// </summary>
    class YueyeTank : Tank
    {
        public override void Go()
        {
            Console.WriteLine("我是一颗中国造越野车，来踏平不老实的小邪恶国家！");
        }
    }
    /// <summary>
    /// 主站坦克
    /// </summary>
    class ZhuzhanTank : Tank
    {
        public override void Go()
        {
            Console.WriteLine("我是一颗中国造主站坦克，来踏平不老实的小邪恶国家！");
        }
    }
    #endregion

}
