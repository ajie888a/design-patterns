﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 抽象工厂模式_Abstract_Factory_
{
    // 备战
    class Make
    {
        //装备 
        private Bomb bomb;
        private Tank tank;
        //制造加工
        public Make(ChinaFactory factory)
        {
            bomb = factory.CreateBomb();
            tank = factory.CreateTank();
        }

        //开始打仗
        public void WarStar()
        {
            //炸弹类爆炸
            bomb.BaoZha();
            //战车类前进
            tank.Go();
        }

    }

}
